import express from 'express';
import fs from 'fs';
import path from 'path'
import https from 'https';
import { auth, requiresAuth } from 'express-openid-connect'; 
import dotenv from 'dotenv'
dotenv.config()

import { Pool } from 'pg'

const app = express();
app.set("views", path.join(__dirname, "views"));
app.set('view engine', 'pug');


const externalUrl = process.env.RENDER_EXTERNAL_URL;
const port = externalUrl && process.env.PORT ? parseInt(process.env.PORT) : 4080;

const config = { 
  authRequired : false,
  idpLogout : true, //login not only from the app, but also from identity provider
  secret: process.env.SECRET,
  baseURL:  externalUrl || `https://localhost:${port}`,
  clientID: process.env.CLIENT_ID,
  issuerBaseURL: 'https://dev-k7agtw8lpentcvyy.eu.auth0.com',
  clientSecret: process.env.CLIENT_SECRET,
  authorizationParams: {
    response_type: 'code' ,
    //scope: "openid profile email"   
   },
};
// auth router attaches /login, /logout, and /callback routes to the baseURL
app.use(auth(config));

const pool = new Pool({
  user: process.env.DB_USER,
  host: process.env.DB_HOST,
  database: 'web2lab1pr8baza',
  password: process.env.DB_PASSWORD,
  port: 5432,
  ssl : true
  })


  export async function getComments() {
  const comments : string[] = [];
  const results = await pool.query('SELECT id, comment from comments');
  results.rows.forEach(r => {
  comments.push(r["comment"]);
  });
  return comments;
}

export async function getUtakmice() {
  const utakmice : string[] = [];
  const kolo : string[] = [];
  const rezultat : string[] = [];
  
  
  


  const results = await pool.query('SELECT naziv, rezultat, kolo from utakmica');
  
  console.log("Database resposne ")
  console.log( results)


  results.rows.forEach(r => {
    utakmice.push(r["naziv"]);
    });

   results.rows.forEach(r => {
    kolo.push(r["kolo"]);
    });

  results.rows.forEach(r => {
    rezultat.push(r["rezultat"]);
    });

  console.log("Utakmice return")
  console.log( utakmice)
  console.log( kolo)
  console.log( rezultat)

  return [utakmice,kolo,rezultat];
}



app.get('/', async function (req, res) {
  let username : string | undefined;

  var utakmice : string[] = [];
  var kolo : string[] = [];
  var rezultat : string[] = [];


  if (req.oidc.isAuthenticated()) {
    username = req.oidc.user?.name ?? req.oidc.user?.sub;
  }

  const allREsult = await getUtakmice()
  
  utakmice = allREsult[0]
  kolo = allREsult[1]
  rezultat = allREsult[2]

  console.log( utakmice)
  console.log( kolo)
  console.log( rezultat)

  var printrez : string[] = [];

  for (let i = 0; i <= utakmice.length - 1; i++)
        {

          printrez[i] = "Kolo: " + kolo[i] + "  Utakmica: " + utakmice[i] +
          " : " + rezultat [i]

        }


  res.render('index', {username, printrez:printrez});
});

app.get('/private', requiresAuth(), function (req, res) {       
    const user = JSON.stringify(req.oidc.user);      
    res.render('private', {user}); 
});

app.get("/sign-up", (req, res) => {
  res.oidc.login({
    returnTo: '/',
    authorizationParams: {      
      screen_hint: "signup",
    },
  });
});












if (externalUrl) {
  const hostname = '127.0.0.1';
  app.listen(port, hostname, () => {
  console.log(`Server locally running at http://${hostname}:${port}/ and from
  outside on ${externalUrl}`);
  })
}




else {
  https.createServer({
      key: fs.readFileSync('server.key'),
      cert: fs.readFileSync('server.cert')
    }, app)
    .listen(port, function () {
      console.log(`Server running at https://localhost:${port}/`);
    });
}